import mne
from .raw import CocktailSpeechRaw
import eelbrain

maxwell_kwargs = dict(
    calibration='/mnt/obob/staff/thartmann/experiments/markov-speech-online/analysis/assets/maxfilter_cal/sss_cal.dat',
    cross_talk='/mnt/obob/staff/thartmann/experiments/markov-speech-online/analysis/assets/maxfilter_cal/ct_sparse.fif',
)

def get_speech_epochs(subject_id, block_nr,
                      maxfilter=True,
                      trans_maxfilter='default',
                      filter_settings=None,
                      filter_njobs=-1,
                      resample=None,
                      ):
    if filter_settings is None:
        filter_settings = {
            'l_freq': 0.1,
            'h_freq': 30,
        }

    raw = CocktailSpeechRaw(subject_id=subject_id,
                            block_nr=block_nr,
                            preload=True)
    this_metadata = raw.evt_metadata.query('is_target_speaker==True')
    this_events = raw.events[this_metadata.index, :]
    for evt, metadata in zip(this_events, this_metadata.iloc()):
        if metadata['has_distractor']:
            evt[0] = metadata['distractor_onset_sample']

    if maxfilter:
        (ch_noise, ch_flat) = mne.preprocessing.find_bad_channels_maxwell(
            raw, **maxwell_kwargs
        )
        raw.info['bads'] = ch_noise + ch_flat

        if trans_maxfilter == 'default':
            destination = (0, 0, 0.04)
        elif trans_maxfilter == 'no':
            destination = None
        else:
            raise RuntimeError('trans value not supported')

        raw = mne.preprocessing.maxwell_filter(
            raw,
            destination=destination,
            **maxwell_kwargs
        )

    raw.pick_types(meg=True, eog=True, ecg=True, misc=True)

    if filter_settings is not False:
        filter_settings['n_jobs'] = filter_njobs
        raw.filter(**filter_settings)

    speech_epochs = []

    for trial_nr in range(2):
        cur_metadata = this_metadata.iloc[trial_nr:trial_nr + 1]
        cur_events = this_events[trial_nr:trial_nr + 1, :]
        this_epoch = SpeechEpoch(
            raw=raw,
            events=cur_events,
            metadata=cur_metadata,
            tmin=-1,
            tmax=cur_metadata['stop_rel_time'].values[0]
        )

        if resample is not None:
            this_epoch.load_data().resample(resample)

        speech_epochs.append(this_epoch)

    return speech_epochs


class SpeechEpoch(mne.Epochs):
    @property
    def eelbrain_time_dim(self):
        return eelbrain.UTS(
            tmin=self.times.min(),
            tstep=1./self.info['sfreq'],
            nsamples=self.times.shape[0]
        )

    @property
    def eelbrain_meg_sensor_dim(self):
        meg_channel_names = [x['ch_name'] for x in self.info['chs'] if
                             x['ch_name'].startswith('MEG')]
        meg_channel_locations = [x['loc'][:3] for x in self.info['chs']
                                 if x['ch_name'].startswith('MEG')]

        return eelbrain.Sensor(meg_channel_locations, meg_channel_names)

    @property
    def eelbrain_meg_data(self):
        meg_data = self.copy().load_data().pick('meg').get_data()
        return eelbrain.NDVar(
            meg_data[0, :, :],
            (self.eelbrain_meg_sensor_dim, self.eelbrain_time_dim),
            name='brain'
        )

    @property
    def eelbrain_target_envelope_data(self):
        envelope_tmp_data = self.get_data('envelope_target')
        return eelbrain.NDVar(envelope_tmp_data[0, 0, :],
                              (self.eelbrain_time_dim, ),
                              name='target_envelope')

    @property
    def eelbrain_distractor_envelope_data(self):
        envelope_tmp_data = self.get_data('envelope_distractor')
        return eelbrain.NDVar(envelope_tmp_data[0, 0, :],
                              (self.eelbrain_time_dim,),
                              name='distractor_envelope')

    @property
    def eelbrain_eyetracker_data(self):
        eye_data = self.get_data(['eyetracker_x', 'eyetracker_y'])
        return eelbrain.NDVar(
            eye_data[0, :, :],
            (eelbrain.Categorial('eyes', ['eyetracker_x', 'eyetracker_y']), self.eelbrain_time_dim)
        )
