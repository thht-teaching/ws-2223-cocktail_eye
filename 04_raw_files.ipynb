{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "84e4de93-3b50-46bd-b605-ee0c1a7c4a10",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "# How to read and process raw data\n",
    "\n",
    "In this file, I am going to show you how to read a *raw* data file. Raw data files are the files that we get from the MEG.\n",
    "\n",
    "In order to do all the analysis, we use an open source Python toolbox called [MNE-Python](https://mne.tools). Remember? You already installed it!\n",
    "\n",
    "[MNE-Python](https://mne.tools) offers convenient functions and methods to analyze MEG and EEG data. Take a look at the website where you can find lots of tutorials and, most importantly, the so-called [Reference](https://mne.tools/stable/python_reference.html). The Reference describes all the functions and clases MNE-Python has to offer.\n",
    "\n",
    "## Import packages\n",
    "\n",
    "So, as for `numpy` in the previous example, we need to import it first. We are also going to import the `RawSounds` class from the `helpers.raw` module that is shipped with this code. It extends the `Raw` class provided by MNE-Python by already adding trigger/event handling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a65c855c-e785-4f5a-a037-eb619a28b0ab",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "from helpers.raw import RawSounds\n",
    "import mne\n",
    "\n",
    "# We need this so that plots appear in separate windows.\n",
    "%matplotlib qt"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "6160c684",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Read the raw data\n",
    "\n",
    "The following code reads the raw data file and puts it into a `RawSounds` object.\n",
    "\n",
    "### Interlude: Objects and classes\n",
    "\n",
    "Python is a so-called object-oriented language. And MNE-Python uses this concept extensively. So, what are objects?\n",
    "\n",
    "Objects are variables that have so-called `properties` and `methods`. Properties are basically sub-variables. Methods are like functions that process the object they are attached to.\n",
    "\n",
    "Do not worry if this is not clear yet. We will use the raw data object we are about to create as an example.\n",
    "\n",
    "But first, we need to create the object. Remember that we imported the `RawSounds` class above? We create an object of it by just calling it like a function with the path to the file as the parameter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ae6e11c0-6234-4eb3-abac-ee85e95cfa06",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data = RawSounds('data/raw/raw_data.fif', preload=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "daafd2fb",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Exploring the raw object.\n",
    "\n",
    "Take a look at the [documentation for the Raw class in the MNE-Python website](https://mne.tools/stable/generated/mne.io.Raw.html). They list all the `properties` (they call them `attributes`) and methods the class has.\n",
    "\n",
    "In order to get an overview, what we loaded here. Let's get an overview:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4fe9441",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3b774a71",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Let's take a look at the data\n",
    "\n",
    "But what is in our data? How does it look like? Luckily, there is a method called `plot` that does that for us:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f8183022-23e4-4e0d-9ebc-7532e38d1d70",
   "metadata": {
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "961438a3",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "But where are our triggers? The raw class we use actually adds an extra property called `events` and the `plot` methods accepts a corresponding parameter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57a39511",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data.plot(events=data.events)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a229ceef",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "The `Raw` object has some more really interesting properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51796305",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "print(data.times)\n",
    "print(data.ch_names)\n",
    "print(data.info)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "719b84d7",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Want a direct look at the data?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1cb85f34",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "print(data.get_data().shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45d4dbc8",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "Note that the parentheses! `get_data` is a method of the object, not a property!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd4268e5",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Filtering\n",
    "\n",
    "One handy and often used method is the [filter method](https://mne.tools/stable/generated/mne.io.Raw.html#mne.io.Raw.filter):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "42aeda3c",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "filtered_data = data.copy()\n",
    "filtered_data.filter(l_freq=1, h_freq=10)\n",
    "filtered_data.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "197b8ddd",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Maxwell Filtering\n",
    "\n",
    "As you might have seen, the MEG data is rather noisy, especially if the projections were kept in. Removing those projections gets us pretty far, but lots of noise still remains in the data. Additionally, there might be some channels that are too noisy that we want to remove.\n",
    "\n",
    "The people at Elekta (the manifacturer of our MEG) developed a nice technique called SSS (subspace separation) or maxfilter. And it is implemented in MNE-Python:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1bdd2ca8",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "(ch_noise, ch_flat) = mne.preprocessing.find_bad_channels_maxwell(data)\n",
    "data.info['bads'] = ch_noise + ch_flat\n",
    "\n",
    "data_maxfiltered = mne.preprocessing.maxwell_filter(data)\n",
    "\n",
    "data_maxfiltered.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55511a55",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Triggers and Events\n",
    "\n",
    "So, looking at the raw data is great. But we want to see what happens when the subject heard a sound. So, we need to know when what happened.\n",
    "\n",
    "You probably remember that we recorded \"Triggers\" with the data.\n",
    "The events are stored in the `data` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f746efa",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data_maxfiltered.events"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dd6aa66f",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "This is basically a table with three columns. You can ignore the middle column. The first column specifies *when* the event took place in samples. The last column holds the trigger value. This specifies, what sound was played and whether it was tuned or detuned.\n",
    "\n",
    "Dealing with these raw values is not really intuitive. Luckily, there is another property called `evt_metadata`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f49219f5",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "data_maxfiltered.evt_metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60016661",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "This table also tells you if the tone was detuned or not, what frequency it had and if the markov sequence was random or ordered.\n",
    "\n",
    "The condition columns all have the same value because as you might remember, we used a block design for these factors and we only read the raw data of one block."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2bfc014a",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## Creating epochs\n",
    "\n",
    "What we actually need for our analysis is not the raw and continous data. We want to cut out the data around the relevant events.\n",
    "\n",
    "Once we have that, we can average them our classifiy them according to the frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b89827a7",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "epochs = mne.Epochs(raw=data_maxfiltered,\n",
    "                    events=data.events,\n",
    "                    metadata=data.evt_metadata,\n",
    "                    tmin=-0.2, tmax=0.7)\n",
    "\n",
    "epochs"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d6c13d9",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## What do we have here?\n",
    "\n",
    "As you can see above, we now have 2000 epochs in our `epochs` object.\n",
    "\n",
    "To keep things simple, we can just average them all and plot them:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "95ecde44",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "avg = epochs.average()\n",
    "avg.plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7f49970",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "## How to choose a subset of epochs\n",
    "\n",
    "So, what we see is an average of all the epochs. But this is not really what we want, is it? We want to be able to access individual epochs and maybe also choose to only use the ones of a particular frequency and condition.\n",
    "\n",
    "First, an `Epochs` object can be accessed like any list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b91288e9",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "epochs[0] # The first\n",
    "epochs[:100] # The first hundred"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41183df1",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "But we can become more advanced! Remember the `evt_metadata` property of the `Raw` object? This has turned into the `metadata` object of the `Epoch`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aed3e672",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "epochs.metadata"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4bcd7512",
   "metadata": {
    "pycharm": {
     "name": "#%% md\n"
    }
   },
   "source": [
    "And we can use this information to extract only the epochs we need."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "235111d2",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "epochs['freq==1']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "756d18e7",
   "metadata": {
    "jupyter": {
     "outputs_hidden": false
    },
    "pycharm": {
     "name": "#%%\n"
    }
   },
   "outputs": [],
   "source": [
    "epochs['freq==1'].average().plot()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "75117fe0",
   "metadata": {},
   "source": [
    "## Let's load the cocktail data\n",
    "\n",
    "As you have seen, the data we loaded is not from the cocktail party experiment... Let's load some of that but first free up some RAM..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5e7b166",
   "metadata": {},
   "outputs": [],
   "source": [
    "from helpers.raw import CocktailSpeechRaw\n",
    "\n",
    "del data\n",
    "del data_maxfiltered\n",
    "\n",
    "data = CocktailSpeechRaw('data/raw/raw_data_cocktail.fif')"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "40407cc1",
   "metadata": {},
   "source": [
    "## And let' take a quick look at it.\n",
    "\n",
    "See how we already added the envelope and eyetracker data..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d484cbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.plot(events=data.events)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd7ef5e6",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  },
  "vscode": {
   "interpreter": {
    "hash": "9680e40163032a4014edb78e5dc96dc6cd0b8c43413b1f0fa2714fc52619b3e3"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
