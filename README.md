# How to prepare your computer for the analysis session

Please follow these instructions and download everything before you come to the class so we all have a smooth experience.

I tried to make the process as smooth as possible. However, if you have problems, just contact me on monday and we sort it out.

You are going to need ~8GB of free disk space to do this...

## Step 0
If you are already familiar with anaconda python environments and maybe even git:

1. clone this repository. be sure to have installed git-lfs!
2. create the environment
3. launch VS code, be sure to have the python and jupyter extensions installed.
4. enjoy!

If the above is all gibberish to you, go to Step 1!

## Step 1: Download and install the necessary software

### Visual Studio Code
We are going to use Visual Studio Code in this tutorial. Please head over to https://code.visualstudio.com/ and download it. Then install it. You can choose the default options.

After you have opened it, you need to install some so-called extensions. To do this, click on the button that looks like this: ![](img/extension_button.png)

You need to install the following extensions:

1. Jupyter
2. Python
3. Micromamba

### Git and Git-LFS
Please also install:

1. https://git-scm.com/
2. https://git-lfs.github.com/

Especially the first one has a lot of options. Just choose the default ones all the time.

After you have installed everything, please restart your computer.

## Step 2: Get the code and the data
Now it is time to get the code and the data. Luckily, VS Code makes this really easy for you

1. Go to this website: https://gitlab.com/thht-teaching/ws-2223-cocktail_eye
2. At the right, you are going to see a button called "Clone".
   1. Click that one
   2. Then choose the option: "Visual Studio Code (HTTPS)"
3. The browser is going to ask you whether Visual Studio Code is allowed to open the link. Click yes.
4. This will open up Visual Studio Code.
5. Again, confirm the "Allow an extension top open this URI" dialog.
6. It will ask you, where the everything should go. Choose a nice folder for it.
7. It will then start to download the repository. There is no progress bar unfortunately. Depending on your network connection, it might take some time. The whole thing is like 2GB!
8. As soon as it is done, it asks you, whether you want to open the repository.
9. Confirm this. And also confirm that you trust the authors.

You should now see the files in the left panel. It should look similar to this:

![](img/after_pull.png)

## Step 3: Create the "environment"
We now need to install all the necessary software for the analysis. This comes in python packages that are installed in so-called "environments". An environment is basically a python interpreter with all the packages.

In order to do this, follow these steps:

1. Press the following keys: Strg+Shift+p
2. A small prompt should open up at the top of the window.
   1. If this did not work, you can also click on the "View" menu and then choose "Command Palette"
3. Enter `Micromamba`
4. This should bring up an option called `Micromamba: create environment`.
5. Click that option.
6. It is now going to download everything it needs.

**This can take a long time. It probably needs to download 600-800MB and install it. Depending on your internet connection and the speed of your laptop, this might take more than one hour to complete!**

You can follow what it is doing in the text panel that it opens. When it is done, it will display something like:

```
Successfully installed aeppl-0.0.38.....
* Terminal will be reused by other tasks, press any key to close it.

```

## Step 4: Make sure, the environment is used.
In the left panel with the files, you can find a new folder called `micromamba`. This is where all your python stuff went. We now need to tell Visual Studio Code to use it.

1. Open the Command Palette by either pressing Strg+Shift+P or using the `View` menu.
2. You need the command `Python: Select Interpreter`. Click it.
3. Click on the `+ Enter Interpreter Path`.
4. This will open a window to select the `python.exe` file. You find it under `.micromamba/envs/cocktail_eye`. 
5.  Open the Command Palette by either pressing Strg+Shift+P or using the `View` menu.
6.  You need the command `Jupyter: Select Interpreter to Start Jupyter Server`. Click it.
7.  There should be one line pointing to the `python.exe` you just created. Click it.
8. Close any open files you might have and restart Visual Studio Code.

## Step 5: Did it work?
Let's see if it worked!

In the file panel, open the file `01_check_if_it_works.ipynb` by clicking on it.

It should open in the editor now and you should see a button that says: `Run All`. Click it.

It should start working now and after a minute or so, you should see a new window with a line going from the bottom left to the top right. The script should have also created some output.

## Step 6: What now?
If it worked, you are done. If it did not work, let me know as soon as possible. Tell me:

1. What step did not work?
2. All error messages you might have received.
3. Send me a screenshot of the problem.